INSERT INTO Flat(id, flat_Number, occupant_Name, block) VALUES ( '5105','5P105', 'Shailesh Charati', '5');
INSERT INTO Flat(id, flat_Number, occupant_Name, block) VALUES ( '5102','5M102', 'Amarinder Singh', '5');
INSERT INTO Flat(id, flat_Number, occupant_Name, block) VALUES ( '1202','1I202', 'H Srinivasan', '1');

INSERT INTO Batch(id, cut_off_time, delivery_date, status) VALUES ( '1','2020-03-22 18:47:52.69', '2020-03-29 12:00:00.00', 'OPEN');

INSERT INTO Order_Item(id,order_date,paymentID,remarks,status) VALUES ('1','2020-03-29 12:00:00.00','','','NEW');
INSERT INTO FLAT_ORDERS(FLAT_ID,ORDERS_ID) VALUES ('5105','1');


INSERT INTO LINE_ITEM(id,name,description,quantity,unit,price,total) VALUES ('1','Ashirwad Wheat Flour','Ashirwad wheat flour plain','5','KG','0.0','0.0');
INSERT INTO LINE_ITEM(id,name,description,quantity,unit,price,total) VALUES ('2','Patanjali soap','bath soap','2','Ea','0.0','0.0');

INSERT INTO ORDER_ITEM_ITEMS(ORDER_ITEM_ID, ITEMS_ID) VALUES ('1','1');
INSERT INTO ORDER_ITEM_ITEMS(ORDER_ITEM_ID, ITEMS_ID) VALUES ('1','2');