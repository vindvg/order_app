package org.swfactory.orderapp.dto;

import org.springframework.data.repository.CrudRepository;
import org.swfactory.orderapp.model.Batch;

public interface BatchRepository extends CrudRepository<Batch, Long> {

}
