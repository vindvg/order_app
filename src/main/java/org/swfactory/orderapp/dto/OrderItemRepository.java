package org.swfactory.orderapp.dto;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.swfactory.orderapp.model.OrderItem;

public interface OrderItemRepository extends CrudRepository<OrderItem, Long> {

}
