package org.swfactory.orderapp.dto;

import org.springframework.data.repository.CrudRepository;
import org.swfactory.orderapp.model.Flat;

public interface FlatRepository extends CrudRepository<Flat, Long> {

	
}
