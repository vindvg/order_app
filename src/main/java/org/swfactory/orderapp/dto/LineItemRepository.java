package org.swfactory.orderapp.dto;

import org.springframework.data.repository.CrudRepository;
import org.swfactory.orderapp.model.LineItem;

public interface LineItemRepository extends CrudRepository<LineItem, Long> {

}
