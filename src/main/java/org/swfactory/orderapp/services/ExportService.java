package org.swfactory.orderapp.services;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

public interface ExportService {

	void exportToExcel(HttpServletResponse response);
}
