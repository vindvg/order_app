package org.swfactory.orderapp.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.swfactory.orderapp.dto.FlatRepository;
import org.swfactory.orderapp.model.Flat;

@Service
public class FlatServiceImpl implements FlatService {

	@Autowired
	FlatRepository flatRepo;
	
	@Override
	public List<Flat> getFlats() {
		return (List<Flat>) (flatRepo.findAll());
	}

	@Override
	public List<Flat> retrieveFlatsByBlock(int block) {
		return null;
	}
	
	public Optional<Flat> getFlatById(Long id) {
		return flatRepo.findById(id);
	}

	@Override
	public Flat save(Flat flat) {
		return flatRepo.save(flat);
	}

}


