package org.swfactory.orderapp.services;

import java.util.Optional;

import org.swfactory.orderapp.model.LineItem;

public interface LineItemService {
	 Optional<LineItem> findItemById(Long id);
	 boolean saveItem(LineItem item);
	 boolean deleteItem(Long id);
}
