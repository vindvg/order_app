package org.swfactory.orderapp.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.swfactory.orderapp.dto.LineItemRepository;
import org.swfactory.orderapp.model.LineItem;

@Service
public class LineItemServiceImpl implements LineItemService {
	
	@Autowired
	LineItemRepository lineItemRepo;
	
	@Override
	public Optional<LineItem> findItemById(Long id) {
		return lineItemRepo.findById(id);
	}

	@Override
	public boolean saveItem(LineItem item) {
		try {
			LineItem savedItem = lineItemRepo.save(item);
			return true;
		}
		catch (Exception e) {
			System.out.println(" exception encountered : " + e);
			return false;
		}
			
	}

	@Override
	public boolean deleteItem(Long id) {
	
		Optional<LineItem> result = lineItemRepo.findById(id);
		LineItem item = null;
		if(result.isPresent()) {
			item = result.get();
		}
		else {
			return false;
		}
		
		try {
			lineItemRepo.delete(item);
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}

}
