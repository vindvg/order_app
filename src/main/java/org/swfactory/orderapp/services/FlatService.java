package org.swfactory.orderapp.services;

import java.util.List;
import java.util.Optional;

import org.swfactory.orderapp.model.Flat;

public interface FlatService {
	List<Flat> getFlats();
	List<Flat> retrieveFlatsByBlock(int block);
	Optional<Flat> getFlatById(Long id);
	Flat save(Flat flat);
}


