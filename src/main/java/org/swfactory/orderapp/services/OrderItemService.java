package org.swfactory.orderapp.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.swfactory.orderapp.model.OrderItem;


public interface OrderItemService {
	
	OrderItem save(OrderItem order);
}
