package org.swfactory.orderapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.swfactory.orderapp.dto.OrderItemRepository;
import org.swfactory.orderapp.model.OrderItem;

@Service
public class OrderItemServiceImpl implements OrderItemService{

	@Autowired
	OrderItemRepository orderRepo;
	@Override
	public OrderItem save(OrderItem order) {
		return orderRepo.save(order);
	}	

}
