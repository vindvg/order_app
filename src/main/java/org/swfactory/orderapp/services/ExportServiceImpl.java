package org.swfactory.orderapp.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.swfactory.orderapp.dto.BatchRepository;
import org.swfactory.orderapp.model.Batch;
import org.swfactory.orderapp.model.BatchStatus;

@Service
public class ExportServiceImpl implements ExportService{
	
	@Autowired
	BatchRepository batchRepo;
	
	InputStream iStream;
	ServletOutputStream oStream;
	
	
	
	public void exportToExcel(HttpServletResponse response){
		List<Batch> batchList = (List<Batch>) batchRepo.findAll();
		
		String fileName = "BatchDetails.xls";
		
		//response.setContentType("application/vnd.ms-excel");
        //response.setHeader("Content-disposition", "attachment; filename=" + fileName);
        response.setContentType("application/force-download");
        response.setHeader("Content-disposition", "attachment; filename=" + fileName);
        //response.addHeader("Content-Disposition", String.format("attachment; filename=\"%s.xls\"", reportName));
		
	    try {
	    		iStream= ExportServiceImpl.class.getResourceAsStream("/templates/spreadSheetTemplate/BatchDetailsTemplate.xlsx");
	    		
	            
	    		oStream = response.getOutputStream();//new FileOutputStream("target/BatchDetails.xls");
	            Context context = new Context();
	            context.putVar("batches", batchList);
	            JxlsHelper.getInstance().processTemplate(iStream, oStream, context);
	            
	            //response.set
	            
	    }catch(IOException ioe){
	    	ioe.printStackTrace();
			
		} finally{
			try{
	        	if(iStream != null) iStream.close();
	        	if(oStream != null) oStream.close();
			}catch(IOException ioe){
	    	    	ioe.printStackTrace();
	        	
	        }
	    }
	}

}
