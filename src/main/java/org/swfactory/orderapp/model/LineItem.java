package org.swfactory.orderapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Required;

@Entity
public class LineItem {

	@Id
	@GeneratedValue
	private long id;
	@NotBlank
	private String name;
	private String description;
	@NotBlank
	private int quantity;
	@NotBlank
	private String unit;
	private double price;
	private double total;
	

	public LineItem() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int qty) {
		this.quantity = qty;
	}
	
	
}
