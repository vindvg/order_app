package org.swfactory.orderapp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ORDER_ITEM")
public class OrderItem {
	
	
    //@JoinColumn(name = "post_id", nullable = false)
	@ManyToOne(targetEntity = Batch.class, fetch = FetchType.LAZY, optional = false)
	@JoinTable(name = "BATCH", joinColumns = {@JoinColumn(name = "id")})
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String paymentID;
	private String remarks;
	private Date orderDate;
	@Enumerated(EnumType.STRING)
	private OrderStatus status;
	@OneToMany(targetEntity = LineItem.class)
	private List<LineItem> items;
	
	
	
	
	public OrderItem() {
		this.orderDate = new Date();
	}


	public String getPaymentID() {
		return paymentID;
	}


	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public OrderStatus getStatus() {
		return status;
	}


	public void setStatus(OrderStatus status) {
		this.status = status;
	}


	public long getId() {
		return id;
	}


	public List<LineItem> getItems() {
		return items;
	}


	public Date getOrderDate() {
		return orderDate;
	}


	public void setId(long id) {
		this.id = id;
	}
}
