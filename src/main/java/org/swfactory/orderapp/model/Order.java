package org.swfactory.orderapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Order {

	@Id
	@GeneratedValue
	private long id;
	private String remarks;
	
	public Order() {
		
	}
}
