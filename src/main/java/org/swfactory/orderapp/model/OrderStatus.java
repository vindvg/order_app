package org.swfactory.orderapp.model;

public enum OrderStatus {

	NEW,
	REVIEW,
	PAYMENT,
	ORDERED,
	DELIVERED,
	CANCELLED
	
}
