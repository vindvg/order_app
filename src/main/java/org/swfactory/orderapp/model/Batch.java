package org.swfactory.orderapp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "BATCH")
public class Batch {
	
	//@OneToMany(fetch = FetchType.LAZY)
	//@JoinTable(name = "ORDER_ITEM", joinColumns = {@JoinColumn(name = "id")})
	@Id
	@GeneratedValue
	private long id;
	private Date deliveryDate;
	private Date cutOffTime;
	@Enumerated(EnumType.STRING)
	private BatchStatus status;
	@OneToMany(targetEntity = OrderItem.class)
	private List<OrderItem> orders;
	
	public Batch() {
		
	}

	public Date getCutOffTime() {
		return cutOffTime;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public void setOrders(List<OrderItem> orders) {
		this.orders = orders;
	}

	public void setCutOffTime(Date cutOffTime) {
		this.cutOffTime = cutOffTime;
	}

	public BatchStatus getStatus() {
		return status;
	}

	public void setStatus(BatchStatus status) {
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public List<OrderItem> getOrders() {
		return orders;
	}
	
	
}
