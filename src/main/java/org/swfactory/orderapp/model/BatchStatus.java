package org.swfactory.orderapp.model;

public enum BatchStatus {
	NEW,
	OPEN,
	CLOSED
}
