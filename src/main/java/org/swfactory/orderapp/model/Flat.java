package org.swfactory.orderapp.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Flat {

	@Id
	@GeneratedValue
	private long id;
	private String flatNumber;
	private String occupantName;
	private int block;
	@OneToMany(targetEntity = OrderItem.class)
	private List<OrderItem> orders;
	
	public Flat() {
		
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public String getOccupantName() {
		return occupantName;
	}

	public void setOccupantName(String occupantName) {
		this.occupantName = occupantName;
	}

	public long getId() {
		return id;
	}

	public int getBlock() {
		return block;
	}

	public List<OrderItem> getOrders() {
		return orders;
	}
}
