package org.swfactory.orderapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwfactoryOrderappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwfactoryOrderappApplication.class, args);
	}

}
