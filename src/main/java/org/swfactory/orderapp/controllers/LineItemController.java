package org.swfactory.orderapp.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.swfactory.orderapp.dto.FlatRepository;
import org.swfactory.orderapp.model.Flat;
import org.swfactory.orderapp.model.LineItem;
import org.swfactory.orderapp.model.OrderItem;
import org.swfactory.orderapp.model.OrderStatus;
import org.swfactory.orderapp.services.FlatService;
import org.swfactory.orderapp.services.LineItemService;
import org.swfactory.orderapp.services.OrderItemService;

@Controller
public class LineItemController {

	@Autowired
	LineItemService lineItemSvc;
	
	@Autowired
	FlatService flatSvc;
	
	@Autowired
	OrderItemService orderSvc;
	
	@GetMapping("/lineitems/{itemid}")
	public String showEditLineItemForm(@PathVariable ("itemid") Long itemid, Model model )
	{
		Optional<LineItem> found = lineItemSvc.findItemById(itemid);
		if(found.isPresent()) {
			LineItem item = found.get();
			model.addAttribute("name", item.getName());
			model.addAttribute("description", item.getDescription());
			model.addAttribute("quantity", item.getQuantity());
			model.addAttribute("unit", item.getUnit());
			model.addAttribute("id",item.getId());
			model.addAttribute("lineItem", item);
		}
		return "editlineitem";
	}
	
	@PostMapping("/lineitems/{itemid}")
	public String saveItem(LineItem item, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("error!!");
		}
		
//		LineItem nItem = new LineItem();
//		nItem.setName("Oodinakaddi");
//		nItem.setDescription("Darshan oodinakaddi");
//		nItem.setQuantity(4);
//		nItem.setUnit("Each");
//		
//		lineItemSvc.saveItem(nItem);
//		Flat newFlat = new Flat();
//		newFlat.setFlatNumber("5L101");
//		newFlat.setOccupantName("Archana Charati");
//		flatSvc.save(newFlat);
		OrderItem newOrder = new OrderItem();
		newOrder.setRemarks("dummy order");
		newOrder.setId(2);
		newOrder.setStatus(OrderStatus.NEW);
		orderSvc.save(newOrder);
		return "orderdetails";
	}
	
	
}
