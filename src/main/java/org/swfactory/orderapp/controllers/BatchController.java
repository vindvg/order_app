package org.swfactory.orderapp.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.swfactory.orderapp.dto.BatchRepository;
import org.swfactory.orderapp.model.Batch;
import org.swfactory.orderapp.model.BatchStatus;

@RestController
public class BatchController {
	
	@Autowired
	BatchRepository batchRepo;
	Batch btch;
	
	@GetMapping("/batches")
	public List<Batch> getAllBatches() {
		return (List<Batch>) batchRepo.findAll();
	}
	
	@PostMapping(value = "/batch")
	
	public void createBatch(@RequestBody Batch btch, BindingResult result){
		//btch.setCutOffTime(new Date());
		//btch.setStatus(BatchStatus.NEW);
		batchRepo.save(btch);
	}
	
}
