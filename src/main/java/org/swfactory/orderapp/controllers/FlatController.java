package org.swfactory.orderapp.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.swfactory.orderapp.dto.FlatRepository;
import org.swfactory.orderapp.model.Flat;
import org.swfactory.orderapp.model.OrderItem;
import org.swfactory.orderapp.model.OrderStatus;
import org.swfactory.orderapp.services.FlatService;

@Controller
public class FlatController {
	@Autowired
	FlatService flatSvc;
	
	@RequestMapping("/flats")
	public List<Flat> getAllFlats() {
		return flatSvc.getFlats();
	}

	@GetMapping("/flatdetails")
	public String getFlatDetails() {
		return "flatdetails";
	}
	
	@PostMapping("/verifyFlatDetails")
	public String verifyFlatDetails(Model model) {
		return "redirect:flatdashboard";
	}
	
	@GetMapping("/flatdashboard")
	public String getDashBoard(Model model) {
		Optional<Flat> myFlat = flatSvc.getFlatById(5105L);
		Flat curFlat = myFlat.get();
		List<OrderItem> orders = curFlat.getOrders();
		List<OrderItem> openOrders = orders.stream().filter(order ->order.getStatus()!= OrderStatus.DELIVERED).collect(Collectors.toList());
		model.addAttribute("orders", openOrders);
		model.addAttribute("flatnumber", "5P105");
		return "flatdashboard";
	}
	
}
