package org.swfactory.orderapp.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.swfactory.orderapp.dto.OrderItemRepository;
import org.swfactory.orderapp.model.LineItem;
import org.swfactory.orderapp.model.OrderItem;

@Controller
public class OrderItemController {

	@Autowired
	OrderItemRepository orderRepo;
	
	@GetMapping("/orders/{orderId}")
	public String getLineItems(@PathVariable("orderId") Long orderId, Model model) {
		Optional<OrderItem> orders = orderRepo.findById(orderId);
		OrderItem curOrder = null;
		if(orders.isPresent()) {
			curOrder = orders.get();
		}
		List<LineItem> items = curOrder.getItems();
		model.addAttribute("orderlineitems",items);
		model.addAttribute("flatnumber","5P105");
		return "orderdetails";
	}
	
	@PostMapping("/order")
	public void createOrder(@RequestBody OrderItem order){
		
		orderRepo.save(order);
		
	}
	
}
